import mongoose , { Schema } from 'mongoose';

const ProductSchema = new Schema({
    name: String, 
    price: Number, 
    price_discount: Number,
    images: [String],
    discount: Number,
},{
    versionKey: false // You should be aware of the outcome after set to false
});
ProductSchema.set('toJSON', {virtuals: true});
export default mongoose.model('Product', ProductSchema);