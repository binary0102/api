import mongoose from "mongoose";
import bcrypt from 'bcrypt'
const Schema = mongoose.Schema;

const UserSchema = new Schema({

    username: { 
        type: String, 
    },
    email: { 
        type: String, 
    },
    password: { 
        type: String, 
    },
    created_at: { 
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
        
});

UserSchema.set('toJSON', {virtuals: true});
UserSchema.method.comparePassword = function(password) {
    return bcrypt.compare(password, this.password);
}
module.exports = mongoose.model('User', UserSchema);
