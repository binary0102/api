import Customer from '../model/customer';
import User from '../model/user';
import Product from '../model/product';
import product from '../model/product';
const resolver = {
  customers: async () => {
    return await Customer.find({});
  },
  hello: () => 'Hello world!',
  users: async function getUser() {
    return await User.find({});
  },
  products: async function getProduct() {
    return  await Product.find({});
  }

}
export default resolver;