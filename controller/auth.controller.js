import User from '../model/user'
function login(req, res) {
    res.render('auth/login');
}
function postLogin(req, res) {
    let username = req.body.usersname;
    let password  = req.body.password;
    const user =  User.findOne({username});
    console.log(user.username);
    if (!user) {
        res.render('auth/login', {
            errors: [
                'User does not exits .',
            ]
        });
        return;
    }
    if (user.password !== password) {
        res.render('auth/login', {
            errors: [
                'Wrong password. '
            ]
        })
        return;
    }
    
    res.redirect('/users');
};


export {login, postLogin};