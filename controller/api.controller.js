import * as userRepository from '../repositories/user.repository'
import Product from '../model/product';
function getAll(req, res, next){
    userRepository.getAll() .then(users => res.json(users))
    .catch(err => next(err));
}
function getToken(req, res, next) {
    userRepository.authenticate(req.body)
    .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
    .catch(err => next(err));
}
async function getProduct(req,res )  {
 
    
    Product.create({ 
        name: "SSD Kingston UV500 3D-NAND M.2 2280 SATA III 120GB SUV500M8/120G",
        price: 200000,
        price_discount: 1000000,
        images: [
            "product.jpg"
        ],
        discount: 20
    }, (err, product)=>{
        if (err) return handleError(err); 
            res.send(product);
    })
}
export { getAll, getToken, getProduct }