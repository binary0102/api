import User from '../model/user';
function index(req,res) {
    User.find({}, function (err, users) {
       
        res.render('users/index',{users: users});
    });
};
function getById(req,res) {
    let id = req.params.id;

    User.findOne({_id:id}, function (err, user) {
        
        res.render('users/view',{user:user});
    });
}
function getCreate(req,res) {
    res.render('users/create');
}

export {index, getById, getCreate};