import * as Yup from 'yup';
import { AuthServices } from '../services/Auth';
import Customer,{ PROVIDER_ENUM } from '../model/customer';
import { AuthProvider } from '../services/AuthProvider';


export const customerAuth = async (req, res, next) => {
    const token = AuthServices.getTokenFromHeaders(req);
    if (!token) {
        req.user = null;

        return res.sendStatus(401);
    }

    const customer = await Customer.findById(token.id);
    if (!customer) {
        req.user = null;

        return res.sendStatus(401);
    }

    req.user = customer;

    return next();
}
export const create = async (req, res) => {
    const { token, provider } = req.body;

    const bodySchema = Yup.object().shape({
        token: Yup.string().required(),
        provider: Yup.string().
        oneOf(PROVIDER_ENUM).
        required(),
    });

    try {
        await bodySchema.validate({token, provider});
        let data;
        if (provider === 'FACEBOOK') {
             data = await AuthProvider.Facebook.authAsync(token);   
            
        }else if (provider === 'GOOGLE') {
             data = await AuthProvider.Google.AuthAsync(token);

        }else {
            res.sendStatus(400);
        }
       
        const customer = await getOrCreateCustomer(data, provider);

        const jwtToken = AuthServices.createToken(customer);
        res.status(200).json({token : jwtToken});
    }catch(error) {
        res.status(400).json({message: error.message || 'no success '});
    }
}
const buildCustomerInfo = (info, providerName) => {
    let user = {
        email: '',
        firstName: '',
        lastName: '',
        avatarUrl: '',
        provider: {
            uid: '',
            type: '',
        }
    }
    if (providerName === 'GOOGLE') {
        user.provider.uid = info.id;
        user.provider.type = providerName;
        user.firstName = info.given_name;
        user.lastName = info.family_name;
        user.email = info.email;
        user.avatarUrl = info.picture;

    }else if (providerName === 'FACEBOOK' ) {
        
        const [firstName, ...lastName] = info.name.split(' ');
        user.firstName = firstName;
        user.lastName  = lastName.join(' ');
        user.provider.uid = info.id;
        user.avatarUrl = info.picture.data.url;
        user.email = info.email;
        user.provider.type = providerName;
    }

    return user;
}
const getOrCreateCustomer = async (info, providerName) => {
   
    const customerInfo = buildCustomerInfo(info, providerName);
    
    const {provider,...userInfo} = customerInfo;
    try {
        const _customer = await Customer.findOne({ email: customerInfo.email});

        if (!_customer) {
            const customer = await Customer.create({
                ...userInfo,
                provider: [provider],
            });

            return customer;
        }
        const providerExist = _customer.provider.find(
            el => 
                el.uid === customerInfo.provider.uid && 
                el.type === customerInfo.provider.type
        );

        if (providerExist) {
            return _customer;
        }
        _customer.provider.push(customerInfo.provider);
        await _customer.save();

        return _customer;
    } catch (error) {
        throw error;
    }
}
