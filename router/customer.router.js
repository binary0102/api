import express from 'express';

import { create,customerAuth } from '../controller/customer.controller';

const routes = express.Router();

routes.post('/', create);
routes.get('/hello',customerAuth,(req, res) => {
    res.send('This is a secret');
});
export default routes;