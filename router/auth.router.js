import express from 'express'
import * as controller from '../controller/auth.controller';

var router = express.Router();

router.get('/login',controller.login);

router.post('/login',controller.postLogin);
export default router;
