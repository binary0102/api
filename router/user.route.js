import express from 'express'
import * as controller from '../controller/user.controller' 

var router = express.Router();

router.get('/',controller.index);
router.get(':id',controller.getById);
router.get('/cookies', function(req, res, next) {
    res.cookie('user-id',1235);
    res.send('hello');
});
router.get('/create', controller.getCreate);


export default router;