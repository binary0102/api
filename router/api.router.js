import express from 'express';
import * as userController from '../controller/api.controller';
var router = express.Router();

router.get('/',userController.getAll);
router.post('/token/',userController.getToken);
router.get('/:id');
router.post('create');
router.delete(':id');
router.put('/:id')
router.get('/test',userController.getProduct);
export default router;
