import {} from 'dotenv/config'
import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose';
import cookieParser from 'cookie-parser'
import authRouter from './router/auth.router'
import userRouter from './router/user.route'
import apiUser from './router/api.router'
import cors from 'cors';
import CustomerRoutes from './router/customer.router';
import graphqlHTTP from 'express-graphql';
import Schema from './schema';
import Resolver from './resolvers';
const http = require("http");
const socketIo = require("socket.io");
const urlConnect = 'mongodb://0.0.0.0:27017/users';

mongoose.connect(urlConnect, function (err) {
    if (err) throw err;
});

var app = express();
const server = http.createServer(app);
const io = socketIo(server);
io.on("connection", socket => {
  console.log("New client connected"), setInterval(
    () => getApiAndEmit(socket),
    10000
  );
  socket.on("disconnect", () => console.log("Client disconnected"));
});
var corsOptions = {
    origin: '*',
    method: ['POST'],
    allowedHeaders : ['Content-Type', 'Authorization'],
    
}
app.options('/api/',cors());
app.options('/graphql', cors());

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cookieParser());
app.set('view engine', 'pug')
app.set('views','./views');
app.use('/graphql', cors(corsOptions), graphqlHTTP({
  schema: Schema,
  rootValue: Resolver,
  graphiql: true,
}));
app.use('/users', userRouter);
app.use('/auth', authRouter);
app.use('/api',cors(corsOptions), apiUser);
app.use('/api/v1/customers', CustomerRoutes );


app.listen(process.env.PORT, function() {
    console.log("Server listening on port ", process.env.PORT);
})  

