import userModel from '../model/user';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';


async function getAll() {
    return await userModel.find().select('-hash');
}
async function authenticate({username, password}) {
    const user = await userModel.findOne({username});
    
    if (user ) {
        console.log(user);
        const token = jwt.sign({sub: user.id}, process.env.PRIVATE_KEY);
        console.log(jwt.verify(token,process.env.PRIVATE_KEY));
        return token;
    }
}
export {getAll,authenticate};