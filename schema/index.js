import { buildSchema } from 'graphql';

var schema = buildSchema(`
  type Query {
    users: [User]
    customers: [Customer]
    hello: String
    products: [Product]
  }
  type User {
    _id: String!
    email: String!
    username: String!
    password: String! 
    created_at: String!
    updated_at: String!
  }
  type Provider {
    uid: String!
    type: String!
  }
  type Customer {
    _id: String!
    firstName: String!
    lastName: String!
    email: String! 
    avatarUrl: String!
    providers: Provider
  }
  type Product {
    _id: String!
    name: String! 
    price: Int! 
    price_discount: Int!
    discount: Int!
  }
  
`);

export default schema;